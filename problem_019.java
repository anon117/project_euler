class problem_019
{
	static int[] honapok = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
	static int[] napok = {0,1,2,3,4,5,6};

	public static void main(String[] args)
	{
		System.out.println(isLeapYear(2100));
		System.out.println(daysInYear2("19990123", "19991231"));

		int napokSzama = 0;

		for (int y = 1901; y <= 2000; y++)
		{
			napokSzama += daysOfYear(y);
		}

		System.out.println(napokSzama);

		

		
	}

	public static Boolean isLeapYear(int bemenet)
	{
		int Year = bemenet;
		if ((Year % 4 == 0) && ((Year % 100 != 0) || (Year % 400 == 0)))
		{	
			return true;
		}
		else
		{	
			return false;	
		}	
	}

	public static int daysOfYear(int bemenet)
	{
		int sum = 0;;
		for (int i = 0; i <= 11; i++)
			sum += honapok[i];

		if (isLeapYear(bemenet))
		{
			return sum + 1;
		}
		else
		{
			return sum;
		}	
	}

	public static int daysInYear2(String eleje, String vege)
	{
		int osszesnap = 0;
		
		int eleje_ev = Integer.parseInt(eleje.substring(0,4));
		int eleje_ho = Integer.parseInt(eleje.substring(4,6));
		int eleje_nap = Integer.parseInt(eleje.substring(6,8));

		int vege_ev = Integer.parseInt(vege.substring(0,4));
		int vege_ho = Integer.parseInt(vege.substring(4,6));
		int vege_nap = Integer.parseInt(vege.substring(6,8));

		int koztes_evek_napjai = 0;
			for (int i = eleje_ev + 1; i <= vege_ev - 1; i++)
				koztes_evek_napjai += daysOfYear(i);

		int toredek_eleje = 0;
		int toredek_vege = 0;

		if (eleje_nap == honapok[eleje_ho - 1])
		{	
			toredek_eleje = honapok[eleje_ho - 1];
		}
		else
		{
			toredek_eleje = honapok[eleje_ho - 1] - eleje_nap;
		}

		if (vege_nap == honapok[vege_ho - 1])
		{	
			toredek_vege = honapok[vege_ho - 1];
		}
		else
		{
			toredek_vege = honapok[vege_ho - 1] - vege_nap;
		}
		


		return toredek_vege;
	}



}