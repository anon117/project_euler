/*
Starting with the number 1 and moving to the right in a clockwise direction a 5 by 5 spiral is formed as follows:

21 22 23 24 25
20  7  8  9 10
19  6  1  2 11
18  5  4  3 12
17 16 15 14 13

It can be verified that the sum of the numbers on the diagonals is 101.

What is the sum of the numbers on the diagonals in a 1001 by 1001 spiral formed in the same way?

*/

class problem_028
{

	static int dim = 1001;
	static int[][] spiral = new int[dim][dim];
	public static void main(String[] args)
	{
		print(spiralNegyzet(dim));
		System.out.printf("\nDiagonals sum: %d\n", diagonal_sum(spiral) - 1); // center not again
	}

	public static int diagonal_sum(int[][] array)
	{
		int sum = 0;

		for (int i = 0; i < dim; i++)
		{
			sum += array[i][i];
			sum += array[dim-1-i][i];
		}

		return sum;
	}

	public static int[][] spiralNegyzet(int dim)
	{
		int oldalhossz = dim;
		int szamlalo = dim*dim;
		int koncentrikusNegyzetekSzama = (int) Math.ceil((dim) / 2.0);
		int j;

		for (int i = 0; i < koncentrikusNegyzetekSzama; i++)
		{
			// teteje
			for (j = 0; j < oldalhossz; j++)
				spiral[i][oldalhossz - 1 - j + i] = szamlalo--;
			// le
			for (j = 1; j < oldalhossz; j++)
				spiral[i + j][i] = szamlalo--;
			// alja
			for (j = oldalhossz - 2; j > -1; j--)
				spiral[dim - 1 - i][dim - j - i - 1] = szamlalo--;
			// fel
			for (j = oldalhossz - 2; j > 0; j--)
				spiral[i+j][dim - 1 - i] = szamlalo--;

			oldalhossz -= 2;
		}
		return spiral;
	}

	public static void print(int[][] array)
	{
		for (int[] row : array)
		{
			for (int elem : row)
			{
				System.out.printf("%3d ", elem);
			}
		System.out.println();
		}
	}

}
