import java.util.*;

class szorzas
{
	static ArrayList<String> sorok2 = new ArrayList<String>();

	static ArrayList<String> sorok = new ArrayList<String>();
	static String szorzando = "";
	static String szorzo = "";
	static String Product = "";

	public static String myBigSor(String aa, String bb) // szorzando, '1'-szamjegy
	{
		
		String be1 = aa;
		String be2 = bb;
		int carry = 0;
		String kimenet = "";
		String ss = "";

		int be1_hossz = be1.length();

		// 1 digit
		int szorzo = Integer.parseInt(be2);
		
		// A szorzand számjegyeinek végigjárása...
		for (int a = be1_hossz; a >= 1; a--)
		{
			Character c = be1.charAt(a-1);
			String s = Character.toString(c);
			
			// digit lesz az aktuális számjegy a szorzandóban
			int digit = Integer.parseInt(s);

			// aktuális érték összeszorozva a szorzóval
			int temp = digit * szorzo;

			if (carry > 0)
				if (temp + carry >= 10)
				{
					int temp2 = (temp + carry) % 10;
					carry = (temp + carry) / 10;
					ss = Integer.toString(temp2);
				}
				else
				{
					int temp2 = (temp + carry) % 10;
					carry = 0;
					ss = Integer.toString(temp2);
				}
			else
				if (temp >= 10)
				{
					int temp2 = temp % 10;
					carry = temp / 10;
					ss = Integer.toString(temp2);
				}
				else
				{
					int temp2 = temp % 10;
					carry = 0;
					ss = Integer.toString(temp2);
				}

			kimenet = ss + kimenet; ss = "";	

		} // végigjárás vége
		if (carry > 0) kimenet = Integer.toString(carry) + kimenet;

		return kimenet;
	
	}

	public static String osszead(String a, String b)
	{
		String aa = a; String bb = b;
		int carry = 0;
		int aa_hossz = aa.length(); int bb_hossz = bb.length();

		int nagyobb = 0;

		int h_nagyobb = (aa_hossz > bb_hossz) ? aa_hossz : bb_hossz; 
		int h_kisebb =  (aa_hossz < bb_hossz) ? aa_hossz : bb_hossz;
		boolean egyenlo = (aa_hossz == bb_hossz) ? true : false;
		int diff = h_nagyobb - h_kisebb;
		
		if (aa_hossz > bb_hossz)
			for (int t = 0; t <= diff - 1; t++)
				bb = "0" + bb;
		else
			for (int t = 0; t <= diff - 1; t++)
				aa = "0" + aa;


		int i_aa = 0; int i_bb = 0; int eredmeny = 0; String kimenet_add = ""; int carry_add = 0;
		for (int t = h_nagyobb - 1; t >= 0; t--)
		{
//				System.out.printf("%s %s - \n", aa.charAt(t), bb.charAt(t));
				Character ch_aa = aa.charAt(t);
				Character ch_bb = bb.charAt(t);
				String s_aa = Character.toString(ch_aa);
				String s_bb = Character.toString(ch_bb);
//	System.out.printf("%s %s * \n", s_aa, s_bb);

				i_aa = Integer.parseInt(s_aa);
				i_bb = Integer.parseInt(s_bb);
//	System.out.printf("%d %d + \n", i_aa, i_bb);

				eredmeny = i_aa + i_bb + carry_add;
//	System.out.printf("%d =\n", eredmeny);

				int mod_eredmeny = eredmeny % 10;
				int osztas_eredmeny = eredmeny / 10;

				if (osztas_eredmeny > 0) carry_add = osztas_eredmeny;
				else carry_add = 0;
//	System.out.printf("%d ----- \n", mod_eredmeny);

				String kimenet_maradek = Integer.toString(mod_eredmeny);

				kimenet_add = kimenet_maradek + kimenet_add;
			

		}
		if (carry_add > 0)
		{
			String kerri = Integer.toString(carry_add);
			kimenet_add = kerri + kimenet_add;
		}
		return kimenet_add;
	}

	
	public static String szorzas(String szorzando, String szorzo)
	{
		int szorzo_hossz = szorzo.length(); String szorzat_eredmeny = ""; String kimenet_szorzat = "";

		for (int ii = szorzo_hossz - 1; ii >= 0; ii--)
		{
			Character _1digit = szorzo.charAt(ii);
			String str_1digit = Character.toString(_1digit);
			szorzat_eredmeny = myBigSor(szorzando, str_1digit);
			sorok2.add(szorzat_eredmeny);
		}

		int szamlalo = 0;
		int sorok_szama = sorok2.size();

		String init_osszeadas = sorok2.get(0);

		for (int uu = 1; uu <= sorok_szama - 1 ; uu++)
		{
			String sor_nullak = sorok2.get(uu);
			for (int nulla = 1; nulla <= uu; nulla++) sor_nullak += "0";
			init_osszeadas = osszead(init_osszeadas, sor_nullak);
		}

		kimenet_szorzat = init_osszeadas;

		return kimenet_szorzat;	
	}

/*
	public static void main(String[] args)
	{

	szorzando = "88888";
	szorzo = "9999";

	System.out.println(szorzas(szorzando, szorzo));
	System.out.println(osszead(szorzando, szorzo));
	}
*/
	
}
