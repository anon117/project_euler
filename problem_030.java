class problem_030
{
	public static void main(String[] args)
	{
		int szam1 = 0;  int szam2 = 0;
		for (Integer i = 2; i < 200000; i++)
		{	szam1 = 0;
			String szam_str = Integer.toString(i);
			int hossz = szam_str.length();

			for (int j = 0; j < hossz; j++)
			{
				Character c = szam_str.charAt(j);
				String s = c.toString();
				int digit = Integer.parseInt(s);

				szam1 += Math.pow(digit, 5);
			}
			
			if (szam1 == i)
			{
				System.out.printf("Egyezés: %d\n", i);
				szam2 += szam1;
			}	
		}
		System.out.printf("\nEzek összege: %d\n", szam2);

	
	}
}
