// The prime factors of 13195 are 5, 7, 13 and 29.
// What is the largest prime factor of the number 600851475143 ?

class problem_003
{
	public static void main(String[] args)
	{
		long[] prim_faktorok = new long[100];
		long be = 600851475143L;
		int i = 0;
		int j;
		int	d = 0;
		long b = 2L;



		while (be > 1)
		{
			if ((be % b) == 0)
			{
				be /= b;
				i++;
			}
			else
			{
				++b;
			}
		}

		System.out.println(b);
		
	}
}

