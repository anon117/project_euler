class problem_014
{
	
	public static void main(String[] args)
	{
		int counter = 0; int old_counter = 0;
		for (int i = 10 ; i < 1000000; i++)
		{
			long n = i;
			
			while(n != 1)
			{
				if (n % 2 == 0)
				{
					n /= 2;
					counter++;
				}
				else
				{
					n = (3 * n) + 1;
					counter++;
				}

			}	
			
			if (counter > old_counter)
			{
				old_counter = counter;
				System.out.printf("\n%d -> [%d]\r", i, counter);
			}
			
			counter = 0;
		}
		System.out.println();
	}
}