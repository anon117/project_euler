/*
Let d(n) be defined as the sum of proper divisors of n (numbers less than n which divide evenly into n).
If d(a) = b and d(b) = a, where a ≠ b, then a and b are an amicable pair and each of a and b are called amicable numbers.

For example, the proper divisors of 220 are 1, 2, 4, 5, 10, 11, 20, 22, 44, 55 and 110; therefore d(220) = 284. The proper divisors of 284 are 1, 2, 4, 71 and 142; so d(284) = 220.

Evaluate the sum of all the amicable numbers under 10000.

*/


class problem_021
{
	public static int d(int i)
	{
		int number = i;
		int out = 0;
		for (int l = 1; l < number; l++)
		{
			if ((number % l) == 0)
				out += l;
		}

		return out;
	}
	
	public static void main(String[] arg)
	{
		int sum = 0;
		for (int x = 1; x < 10000; x++)
		{
			int a = d(x); int b = d(a);
			if (b == x && a != b)
				sum += a;
		}

	System.out.printf("Sum: %d\n", sum);
	}

}
