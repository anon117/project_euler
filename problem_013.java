import java.io.*;
import java.util.*;

class problem_013
{
	static ArrayList<String> sorok = new ArrayList<String>();

	public static void main(String[] args)
	{
		sorok = fileReadLine("problem13.data");
		int meret = sorok.size();

		szorzas s = new szorzas();

		String osszeg = sorok.get(0);

		for (int i = 1; i < meret; i++)
		{
			osszeg = s.osszead(osszeg, sorok.get(i));
		}	

		System.out.println(osszeg.substring(0,10));
	}

	public static ArrayList<String> fileReadLine(String path)
	{
		ArrayList<String> temp = new ArrayList<String>();

		try
		{

			FileInputStream fstream = new FileInputStream(path);

			DataInputStream in = new DataInputStream(fstream);
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			String strLine;

			while ((strLine = br.readLine()) != null)
				temp.add(strLine);
				  
			in.close();
		}
		catch (Exception e)
		{
			System.err.println("Error: " + e.getMessage());
		}

		return temp;
	}	

}