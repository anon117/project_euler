import java.util.*;

class problem_035
{
	static ArrayList<Integer> perm_array = new ArrayList<Integer>();
	static ArrayList<Integer> temp_array = new ArrayList<Integer>();
	
	public static void main(String[] args)
	{
		int circ_counter = 0;
		perm_array = szures(1,1);
		for (Integer prim: perm_array)
		{
			int prime_rotate_counter = 0;
			String str_prim = Integer.toString(prim);
			int str_prim_hossza = str_prim.length();

			if (str_prim_hossza == 1)
			{	
				circ_counter++;
				System.out.printf("Prim: %d\n", prim);
			}	
			else
				for (int k = 0; k < str_prim_hossza; k++)
				{
					Integer vizsgalando = Integer.parseInt(rotateString(str_prim, 1));
					// System.out.printf(" --%d--%d-- ", vizsgalando, prim);
					if (isPrime(vizsgalando))
					{	
						prime_rotate_counter++;
						System.out.printf(" *%d* ", str_prim_hossza);
					}	
				}

//System.out.printf(" ++%d++%d++ ", prime_rotate_counter, str_prim_hossza);

			if (prime_rotate_counter  == str_prim_hossza)
			{
				System.out.printf("Prim: %d\n", prim);
				circ_counter++;
			}
			
		}
		System.out.printf("\n\nCirkuláris primek száma: %d\n", circ_counter);
	}

	/**
	Visszaadja az input string permutációját
	*/
	public static void permutation(String eleje, String input)
	{
		String s = input;
		int hossz = s.length();

		if (hossz <= 1)
			System.out.println(eleje + s);
			
		else
		{
			for (int i = 0; i < hossz ; i++)
			{
				String newString =  s.substring(0, i) + s.substring(i + 1);

				permutation(eleje + s.charAt(i), newString);
			}	
		}		
	}

	/**
	String elforgatása az irány szerint jobbra vagy balra (1,2)
	*/
	public static String rotateString(String bemenet, int direction)
	{
		int bemenet_hossz = bemenet.length();
		String kimenet = "";
		String elso_karakter = "";	
		String utolso_karakter = "";
		String eleje = "";
		String vege = "";

		if (direction == 1)
		{
			eleje = bemenet.substring(0,bemenet_hossz - 1);
			utolso_karakter = bemenet.substring(bemenet_hossz - 1, bemenet_hossz);

			kimenet = utolso_karakter + eleje;
		}	
		if (direction == 2)
		{	
			elso_karakter = bemenet.substring(0,1);
			vege = bemenet.substring(1, bemenet_hossz);

			kimenet = vege + elso_karakter;
		}

		return kimenet;
	}

	/**
	A megadott intervallumban megkeresi a primeket és visszaadja egy tömben.
	*/
	public static ArrayList<Integer> szures(int also, int felso)
	{
		ArrayList<Integer> primek = new ArrayList<Integer>();
		int darab = 100;
		Boolean[] array = new Boolean[darab + 1];
		Arrays.fill(array, Boolean.FALSE);

		array[0] = Boolean.FALSE; array[1] = Boolean.FALSE;

		for (int i = 2; i <= darab - 1; i++)
			array[i] = Boolean.TRUE;

		int j = 2;
		
		while (j*j <= darab)
		{
			for (int k = j*j; k <= darab; k += j)
				array[k] = Boolean.FALSE;
			j++;
		}

		for (int l = 2; l <= array.length-1; l++)
			if (array[l])
				primek.add(l);

		return primek;

	}

	/**
	A megadott számról megmondja, h prime-e? True = igen, az. 2 vagy nagyobb számot adj meg
	*/
	public static Boolean isPrime(Integer bemenet)
	{
		Boolean kimenet = Boolean.FALSE;
		int factor_counter = 0; int i = 0;

		for (int j = 2; j <= bemenet; j++)
			if ((bemenet % j) == 0)
				factor_counter++;
		if (factor_counter == 1)
			return Boolean.TRUE;
		else
			return Boolean.FALSE;

	}

	/**
	
	*/

}