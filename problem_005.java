class problem_005
{
	public static void main(String[] args)
	{
		boolean megvan = false;
		int meddig = 20;
		int szam = 0;
		int modulo_counter = 0;

		while (!megvan)
		{
			szam++;
			for (int i = 1; i <= meddig; i++)
			{
				if ((szam % i) == 0)
					modulo_counter++;
			}
			
			if (modulo_counter == meddig)
				megvan = true;
			
			modulo_counter = 0;
		}
		System.out.printf("Az eredmény: %d\n", szam);
	}

}

/*
real	1m54.622s
user	1m49.051s
sys		0m0.340s
*/
