// (m^2-n^2)^2+(2mn)^2=(m^2+n^2)^2

class problem_009
{
	public static void main(String[] args)
	{
		boolean megvan = false;

		int a = 0; int b = 0; int c = 0;

		while (!megvan)
		{
			for (int n = 1; n <= 999; n++)
			{
				int p = n + 1;
				for (int m = p; m <= 999; m++)
				{
					a = ((m * m) - (n * n)) * ((m * m) - (n * n));
					b = ((2 * m * n) * (2 * m * n));
					c = ((m * m) + (n * n)) * ((m * m) + (n * n));
	
					a = (int) Math.sqrt(a);
					b = (int) Math.sqrt(b);
					c = (int) Math.sqrt(c);

					if ((a + b + c) == 1000) 
					{
						megvan = true; System.out.printf("m:%d n:%d - %d %d %d", m, n, a, b, c);
						System.out.printf(" Szorzat: %d\n", (a*b*c));
					}
				}
			}
		}

	}
}
