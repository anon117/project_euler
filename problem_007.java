// 10 001. prim szám

class problem_007
{
	public static void main(String[] args)
	{
	
		boolean isprime = false;
		long i = 1;
		int hanyadik = 10001;
		int prime_counter = 1;
		int prime_value = 0;
		int factor_counter = 0;

		while (prime_counter <= hanyadik)
		{
			i++;
			for (int j = 2; j <= i; j++)
				if ((i % j) == 0)
					factor_counter++;
			if (factor_counter == 1)
			{
				prime_counter++;
			}
			
			factor_counter = 0;

		}
		System.out.printf("%d. prime number is %d\n", hanyadik, i);
	}
}
