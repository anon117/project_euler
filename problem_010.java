import java.util.*;

class problem_010
{
	public static void main(String[] args)
	{
		int darab = 2000000;
		long szum = 0L;

		Boolean[] array = new Boolean[darab + 1];
		Arrays.fill(array, Boolean.FALSE);

		array[0] = Boolean.FALSE; array[1] = Boolean.FALSE;

		for (int i = 2; i <= darab - 1; i++)
			array[i] = Boolean.TRUE;

		int j = 2;
		System.out.printf("Szűrés... ");
		while (j*j <= darab)
		{
			for (int k = j*j; k <= darab; k += j)
				array[k] = Boolean.FALSE;
			j++;
		}
		System.out.printf("VÉGE\nÖsszegzés... ");
		for (int l = 2; l <= array.length-1; l++)
			if (array[l])
				szum+=l;
		System.out.printf("VÉGE! Eredmény: %d\n", szum);
	}
}
