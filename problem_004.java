// A palindromic number reads the same both ways. The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 × 99.
// Find the largest palindrome made from the product of two 3-digit numbers.

class problem_004
{
	public static void main(String[] args)
	{
		int palindrome = 0;
		int productof = 0; int eredeti = 0;
		int i, j, maradek, mod;
		int loopcount = 0;
		int visszafele = 0;
		int tizhatvanya = 1;

		for (i = 100; i <= 999; i++)
			for (j = 100; j <= 999; j++)
			{
				productof = i * j;
				eredeti = productof;
				visszafele=0; loopcount=0;
				while (productof > 0)
				{
					maradek = productof % 10;
					mod = productof / 10;
					visszafele = maradek + (10 * visszafele);
					loopcount++;
					productof = mod;
				}

				if (eredeti == visszafele)
				{
					if (visszafele > palindrome)
						palindrome = visszafele;
				}
			}
			System.out.printf("Ez a legnagyobb palindroma: %d\n", palindrome);
	}
}
